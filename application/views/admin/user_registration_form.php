<!DOCTYPE html>
<html lang="en">

    <body>


        <div class="container">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6">
                    <h3>

                    </h3>

                    <form class="form-horizontal" action="<?php echo base_url(); ?>dashboard/user_registration_process" method="post" id="registration_form" >


                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">User Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-text"  placeholder="Enter User Name" name="user_name" id="form_username"><span class="error_form" id="username_error_message" ></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">First Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required="" placeholder="Enter First Name" name="first_name" id="form_firstname"><span class="error_form" id="firstname_error_message"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Last Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control "  placeholder="Enter Last Name" name="last_name" id="form_lastname"><span class="error_form" id="lastname_error_message"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Email</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control "  placeholder="Enter Email" name="email" id="email">
                                <span class="error-form" id="email_result"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control "  placeholder="Enter password" name="password" id="form_password"><span class="error_form" id="password_error_message"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Retype Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control "  placeholder="Enter password" name="password" id="form_retype_password"><span class="error_form" id="retypepassword_error_message"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Phone Number</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control "  placeholder="Enter Phone Number" name="phone_number" id="form_phone_number"><span class="error_form"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Profession</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control error-form"  placeholder="Enter Profession" name="profession" id="form_profession"><span class="error_form"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Birthday</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control "  size="8" id="datepicker" name="birthday" id="form_birthday"><span class="error_form" id="birthday_error_message"></span>


                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Gender</label>
                            <div class="col-sm-8">
                                <input type="radio" name="gender" value="male" checked id="form_male" value="male"> Male<br>
                                <input type="radio" name="gender" value="female" id="form_female" value="female"> Female<br>
                                <input type="radio" name="gender" value="other" id="form_other" value="other" > Other<br><br>
                            </div>
                        </div>




                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success btn-block">Register</button>
                            </div>
                        </div>

                        <h3>
                            <?php
                            echo $this->session->userdata('message');
                            $this->session->unset_userdata('message');
                            ?>
                        </h3>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>






