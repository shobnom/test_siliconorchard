<!DOCTYPE html>
<html lang="en">
<!--    <head>
        <title>Add Table Data</title>
  
    </head>-->
    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6">
                    <h3>
                   <?php 
                   echo $this->session->userdata('message');
                   $this->session->unset_userdata('message');
                   
                   ?>
                    </h3>

                    <form class="form-horizontal" action="<?php echo base_url(); ?>dashboard/add_data" method="post">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  placeholder="Enter Name" name="name" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd" style="padding-left: 3px ">Description:</label>
                            <div class="col-sm-10">          

                                <textarea name="description" cols="30" rows="5" required="" class="textarea_setup">
                                        
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </body>
</html>
