<html>
<?php $this->load->view('admin/header/header'); ?>
    
    <body>
        <div class="page-container">

            <div class="left-content">
                <div class="inner-content">

                    <div class="header-section">

                        <div class="clearfix"></div>
                    </div>

                    <?php echo $main_content; ?>

                </div>
            </div>

            <div class="sidebar-menu">

                <!--//down-->
                <div class="menu">
                    <ul id="menu" >

                        <li><a href="#"> Data Info </a>
                            <ul id="menu-academico-sub" >

                                <li id="menu-academico-boletim" ><a href="<?php echo base_url(); ?>dashboard/add_data_form">Add Data</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>dashboard/manage_data_page">Manage Data</a></li>

                            </ul>
                        </li>

                        <ul>
                            <li><a href="<?php echo base_url(); ?>dashboard/user_registration">User Registration </a></li>
                        </ul>
                        
                         <ul>
                            <li><a href="<?php echo base_url(); ?>user/user_login">Login here </a></li>
                        </ul>

                        <ul>
                            <li><a href="<?php echo base_url(); ?>dashboard/logout">Log Out</a></li>
                        </ul>

                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>		
        </div>

    </body>
</html>