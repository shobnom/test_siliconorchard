<html>



    <body>



        <div class="col-lg-12">
            <div class="container">
                <div class="row">

                    <h3>
                        <?php
                        echo $this->session->userdata('message');

                        $this->session->unset_userdata('message');
                        ?>
                    </h3>
                    <div class="col-lg-12">
                        <table class="table table-bordered table-hover">


                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>action</th>
                            </tr>

                            <?php foreach ($result as $r) { ?>
                                <tr>

                                    <td><?php echo $r->id; ?></td>
                                    <td><?php echo $r->name; ?></td>
                                    <td><?php echo word_limiter($r->description, 10); ?>

                                        <a href="<?php echo base_url(); ?>dashboard/view_details_data/<?php echo $r->id; ?>" class="btn btn-primary" title="view more">
                                            <span class="glyphicon glyphicon-file" ></span>   
                                        </a>

                                    </td>
                                    <td class="text-center">



                                        <a href="<?php echo base_url(); ?>dashboard/edit_data_form/<?php echo $r->id; ?>" class="btn btn-success" title="Edit">
                                            <span class="glyphicon glyphicon-edit"></span>   
                                        </a>
                                        <a href="<?php echo base_url(); ?>dashboard/Delete_data/<?php echo $r->id; ?>" class="btn btn-danger" title="Delete" >                                                                         
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>

                            <?php } ?>

                        </table>

                        <?php
                        echo $this->pagination->create_links();
                        ?>
                    </div>

                </div>
                
            </div>
        </div>

    </body>
</html>
