<!DOCTYPE html>
<html lang="en">
 
    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6">
                    <h3>


                    </h3>

                    <form class="form-horizontal" action="<?php echo base_url(); ?>dashboard/update_selected_data" method="post">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"  placeholder="Enter Name" name="name" value="<?php echo $all_selected_id_info->name; ?>">
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control"  name="id" value="<?php echo $all_selected_id_info->id; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Description:</label>
                            <div class="col-sm-10">          

                                <textarea name="description" cols="30" rows="5">
                                    <?php echo $all_selected_id_info->description; ?>  
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Update</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </body>
</html>
