<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_model.php';

class M_api extends B_model {

    public function __construct() {
        parent::__construct();
    }

    public function get_description($current_name) {

        $this->db->select('description');
        $this->db->from('tbl_data');
        $this->db->where('name', $current_name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
        } else {
            $result = [
                'description' => null,
            ];
        }
        return $result;
    }

}
