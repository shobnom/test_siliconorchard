<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_model.php';

class Admin_Model extends B_model {

    public function admin_login($admin_email_address, $admin_password) {

        $this->db->select('*');
        $this->db->from('tbl_admin');
        $this->db->where('admin_email_address', $admin_email_address);
        $this->db->where('admin_password', md5($admin_password));
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

}
