<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_model.php';

class Data_Model extends B_model {

    public function insert_all_data($Adata) {

        $this->db->insert('tbl_data', $Adata);
    }

//    public function manage_all_data() {
//        
//        $this->db->select('*');
//        $this->db->from('tbl_data');
//        $query_result = $this->db->get();
//        $result = $query_result->result();
//        return $result;
//        
//    }

    public function edit_data_info($id) {

        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function update_data_by_id($name, $description, $id) {


        $this->db->set('name', $name);
        $this->db->set('description', $description);
        $this->db->where('id', $id);
        $this->db->update('tbl_data');
    }

    public function delete_selected_data_by_id($data_id) {



        $this->db->where('id', $data_id);
        $this->db->delete('tbl_data');
    }

    public function count_data() {

        return $this->db->count_all('tbl_data');
    }

    public function fetch_data($limit, $offset) {

        $this->db->limit($limit, $offset);
        $query = $this->db->get('tbl_data');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return $query->result();
        }
    }

    public function manage_details_data($id) {

        $this->db->select('description');
        $this->db->from('tbl_data');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function user_registration_data_insert($Rdata) {

        $this->db->insert('tbl_user_registration', $Rdata);
    }




    public function is_email_available($email) {

        $this->db->where('email', $email);
        $query = $this->db->get('tbl_user_registration');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return FALSE;
        }
    }

}
