<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_model.php';

class User_Model extends B_model {

    public function user_login($user_email, $user_password) {

        $this->db->select('*');
        $this->db->from('tbl_user_registration');
        $this->db->where('email', $user_email);
        $this->db->where('password', $user_password);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

}
