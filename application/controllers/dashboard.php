<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();

        
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id == NULL) {
            redirect('admin');
        }
    }

    public function index() {
        $data = array();
        $data['main_content'] = $this->load->view('admin/home/home_content', '', TRUE);
        $this->load->view('admin/admin_dashboard', $data);
    }
    
   

    public function logout() {

        $this->session->unset_userdata('admin_full_name');
        $this->session->unset_userdata('admin_id');

//        $sdata = array();
//
//        $sdata['message'] = "you are successfully logged out";
//
//        $this->session->set_userdata($sdata);

        redirect('admin');
    }

    public function add_data_form() {

        $data = array();
        $data['main_content'] = $this->load->view('admin/add_data', '', TRUE);

        $this->load->view('admin/admin_dashboard', $data);
    }

    public function add_data() {

        $Adata = array();
        $Adata['name'] = $this->input->post('name', TRUE);
        $Adata['description'] = $this->input->post('description', TRUE);
        $this->data_model->insert_all_data($Adata);

        $Adata['message'] = 'Data added successfully';

        $this->session->set_userdata($Adata);

        redirect('dashboard/add_data_form');
    }

    public function manage_data_page() {



        $this->load->helper('text');
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . 'dashboard/manage_data_page';
        $config['total_rows'] = $this->data_model->count_data();
        $config['per_page'] = 4;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        $data['result'] = $this->data_model->fetch_data($config['per_page'], $this->uri->segment(3));

        $data['message'] = 'Here is your all Data';
        $this->session->set_userdata($data);


        $data['main_content'] = $this->load->view('admin/manage_data', $data, TRUE);


        $this->load->view('admin/admin_dashboard', $data);






//        $Mdata = array();
//        $Mdata['result'] = $this->data_model->manage_all_data();
//        
    }

    public function edit_data_form($id) {

        $Edata = array();
        $Edata['all_selected_id_info'] = $this->data_model->edit_data_info($id);
        $Edata['main_content'] = $this->load->view('admin/edit_data_form', $Edata, TRUE);

        $this->load->view('admin/admin_dashboard', $Edata);
    }

    public function update_selected_data() {

        $name = $this->input->post('name', TRUE);
        $description = $this->input->post('description', TRUE);
        $id = $this->input->post('id', TRUE);


        $this->data_model->update_data_by_id($name, $description, $id);

        $data['message'] = 'Data updated successfully';
        $this->session->set_userdata($data);

        redirect('dashboard/add_data_form');
    }

    public function Delete_data($data_id) {

        $this->data_model->delete_selected_data_by_id($data_id);



        redirect('dashboard/manage_data_page');
    }

    public function view_details_data($id) {


        $Edata['result'] = $this->data_model->manage_details_data($id);
        $Edata['main_content'] = $this->load->view('admin/details_manage_info', $Edata, TRUE);

        $this->load->view('admin/admin_dashboard', $Edata);
    }

    public function user_registration() {

        //$this->load->view('admin/user_registration_form');
        $data = array();
        $data['main_content'] = $this->load->view('admin/user_registration_form', '', TRUE);

        $this->load->view('admin/admin_dashboard', $data);
    }

    public function check_email_availability() {
//        $email = $_POST["email"];
//        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//            echo 'error';
//        } else {
//            echo 'success';
//        }
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            //echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span>Invalid Email</span></label>';
            echo '<span class="text-danger">Invalid Email</span>';
            
        } else {

            if ($this->data_model->is_email_available($_POST["email"])) {
                //echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span>Email Already Register</span></label>';
                echo '<span class="text-danger">Email Already registered</span>';
                
            } else {
                //echo '<label class="text-danger"><span class="glyphicon glyphicon-ok"></span>Email Available</span></label>';
                echo '<span class="text-success">Email Available </span>';
                
            }
        }
    }

    public function user_registration_process() {

        $Rdata = array();
        $Rdata['user_name'] = $this->input->post('user_name');
        $Rdata['first_name'] = $this->input->post('first_name');
        $Rdata['last_name'] = $this->input->post('last_name');
        $Rdata['email'] = $this->input->post('email');
        $Rdata['password'] = $this->input->post('password');
        $Rdata['phone_number'] = $this->input->post('phone_number');
        $Rdata['profession'] = $this->input->post('profession');
        $Rdata['birthday'] = $this->input->post('birthday');
        $Rdata['gender'] = $this->input->post('gender');
        $this->data_model->user_registration_data_insert($Rdata);
        $Rdata['message'] = "You are successfully registered";
        $this->session->set_userdata($Rdata);
        redirect('user/user_login');
    }

//    public function verify_data($email) {
//        
//        $this->data_model->is_email_exist($email);
//        
//    }
}
