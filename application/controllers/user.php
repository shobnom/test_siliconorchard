<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MY_Controller {

    public function user_login() {

        //$this->load->view('admin/user_login_form');

        $data = array();
        $data['main_content'] = $this->load->view('admin/user_login_form', '', TRUE);

        $this->load->view('admin/admin_dashboard', $data);
    }

    public function login() {
        $sdata = array();
        $user_email = $this->input->post('user_email', TRUE);
        $user_password = $this->input->post('user_password', TRUE);


        $result = $this->user_model->user_login($user_email, $user_password);
        if ($result) {

            $sdata = array();

//            $sdata['user_id'] = $result->user_id;
//
//            $this->session->set_userdata($sdata);
            redirect('dashboard');
        } else {

            echo 'sorry!wrong email or password';
        }
    }

}
