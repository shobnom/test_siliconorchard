<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Admin extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $admin_id = $this->session->userdata('admin_id');
        
        if($admin_id != NULL)
        {
            redirect('dashboard');
        }
    }

    public function index() {

        $this->load->view('admin/login');
    }

    public function login() {

        $sdata = array();
        $admin_email_address = $this->input->post('admin_email_address', TRUE);
        $admin_password = $this->input->post('admin_password', TRUE);

        
        $result = $this->admin_model->admin_login($admin_email_address, $admin_password);
        if ($result) {
                 
            $sdata = array();
            
            $sdata['admin_id'] = $result->admin_id;
            
            $this->session->set_userdata($sdata);

            redirect('dashboard');
        } else {
            
         
            redirect('admin');
        }
    }
    
    
   

}
