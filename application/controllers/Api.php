<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Api extends MY_Controller {

    const DATE_FORMAT = 'Ymd His';
    protected $requestMethod;
    protected $json;

    public function __construct() {
        parent::__construct();
        header('Content-Type: application/json');
        $this->requestMethod = $this->input->method(TRUE);
        if (in_array($this->requestMethod, array('POST', 'PUT', 'DELETE'))) {
            $data = $this->input->raw_input_stream;
            $this->json = json_decode($data);
            if (!empty($data) && is_null($this->json)) {
                $this->_throw_exception(2, "Invalid JSON");
            }
        }

        $this->load->model('m_api');
    }

    public function _throw_exception($resultCode, $msg) {

        $result = [
            "resultCode" => $resultCode,
            "time" => date(self::DATE_FORMAT),
            "msg" => $msg
        ];

        echo json_encode($result);
        exit;
    }

    protected function isInt($var) {
        if (is_int($var)) {
            return true;
        }
        return preg_match("/^[0-9]+$/", $var) > 0;
    }

    protected function isValidType($value, $type) {
        $result = FALSE;
        if (is_null($value)) {
            return TRUE;
        } else {
            switch ($type) {
                case 'int':
                    $result = $this->isInt($value);
                    break;
                case 'bool':
                    $result = is_bool($value);
                    break;
                case 'string':
                    $result = is_string($value);
                    break;
                case 'binary':
                    $result = is_binary($value);
                    break;
                default:
                    $result = TRUE;
                    break;
            }
        }
        return $result;
    }

    protected function getValueFromJSON($path, $type, $required = FALSE) {
        if (empty($this->json)) {
            $this->_throw_exception(2, "JSON is empty.");
        }
        if (is_string($path)) {
            $path = array(
                $path
            );
        }
        $pathStr = implode("->", $path);
        $var = $this->json;

        while (!empty($path)) {
            $pathElement = array_shift($path);
            $var = isset($var->$pathElement) ? $var->$pathElement : NULL;
        }
        if (TRUE == $required && is_null($var)) {
            $this->_throw_exception(3, "$pathStr is not set.");
        }
        if (!$this->isValidType($var, $type)) {

            $this->_throw_exception(3, "The type of $pathStr is not valid.");
        }
        return $var;
    }

    function index() {
        $this->_throw_exception(404, "Not Found");
    }

    function get_location_info() {


        $current_name = 'Flash'; //$this->getValueFromJSON('current_name', 'string', TRUE);

        $data = $this->m_api->get_description($current_name);

        $result = [
            "resultCode" => 0,
            "time" => date(self::DATE_FORMAT),
            "description" => $data['description'],
        ];
        
        $test = array(
            "test" => $result
        );

      
        echo json_encode($test);
        
        exit();
    }

}
