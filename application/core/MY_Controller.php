<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My_Controller extends CI_Controller {

    protected $admin = FALSE;
    protected $_limit = 15;
    protected $sort_type = 'ASC';
    protected $sort_column = 'id';
    protected $_sorting = array(
        'column' => 'id',
        'type' => 'ASC',
        'limit' => 15
    );

    function __construct() {
        parent::__construct();
    }

}
