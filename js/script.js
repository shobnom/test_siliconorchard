$(function () {
    $('#username_error_message').hide();
    $('#firstname_error_message').hide();
    $('#lastname_error_message').hide();
    //$('#email_result').hide();
    $('#password_error_message').hide();
    $('#retypepassword_error_message').hide();
    $('#birthday_error_message').hide();


    var error_username = false;
    var error_firstname = false;
    var error_lastname = false;
    //var error_email = false;
    var error_password = false;
    var error_retypepassword = false;
    var error_birthday = false;

    $('#form_username').focusout(function () {

        check_username();

    });

    $('#form_firstname').focusout(function () {

        check_firstname();

    });

    $('#form_lastname').focusout(function () {

        check_lastname();

    });

//    $('#email').focusout(function () {
//
//        check_email();
//
//    });

    $('#form_password').focusout(function () {

        check_password();

    });

    $('#form_retype_password').focusout(function () {

        check_retype_password();

    });

    $('#form_birthday').focusout(function () {

        check_birthday();

    });

    function check_username()
    {
        var username_length = $('#form_username').val().length;
        if (username_length < 5 || username_length > 20)
        {
            $("#username_error_message").html("should be between 5-20 characters");
            $("#username_error_message").show();
            error_username = true;

        }

        else
        {
            $("#username_error_message").hide();
        }
    }

    function check_password()
    {
        var password_length = $('#form_password').val().length;
        if (password_length < 8)
        {
            $("#password_error_message").html("At least 8 characters");
            $("#password_error_message").show();
            error_password = true;
        }
        else
        {
            $("#password_error_message").hide();
        }
    }

    function check_retype_password()
    {
        var password = $('#form_password').val();
        var retype_password = $('#form_retype_password').val();
        if (password != retype_password)
        {
            $("#retypepassword_error_message").html("passwords don't match");
            $("#retypepassword_error_message").show();
            error_retypepassword = true;
        }
        else
        {
            $("#retypepassword_error_message").hide();
        }
    }

//    function check_email()
//    {
//
//        var pattern = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//        if (pattern.test($('#email').val()))
//        {
//            $('#email_result').hide();
//
//        }
//
//        else
//        {
//            $("#email_result").html("Invalid Email Address");
//            $('#email_result').show();
//            error_email = true;
//
//        }
//      
//        
//    }

    function check_firstname()
    {
        var firstname = $('#form_firstname').val();
        if (firstname == '')
        {
            $('#firstname_error_message').html("Please Enter First Name");
            $('#firstname_error_message').show();
            error_firstname = true;
        }
        else
        {

            var pattern = /^[a-zA-Z]+$/;
            if (pattern.test($('#form_firstname').val()))
            {
                $("#firstname_error_message").hide();
            }
            else
            {
                $("#firstname_error_message").html(('Invalid Name'));
                $('#firstname_error_message').show();
                error_firstname = true;
            }
        }
    }


    function check_lastname()
    {
        var lastname = $('#form_lastname').val();
        if (lastname == '')
        {
            $('#lastname_error_message').html("Please Enter Last Name");
            $('#lastname_error_message').show();
            error_lastname = true;
        }
        else
        {
            var pattern = /^[a-zA-Z]+$/;
            if (pattern.test($('#form_lastname').val()))
            {
                $("#lastname_error_message").hide();
            }
            else
            {
                $("#lastname_error_message").html(('Invalid Name'));
                $('#lastname_error_message').show();
                error_lastname = true;
            }
        }
    }



 

});

