

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_full_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(50) NOT NULL,
  `admin_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_full_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'Disha', 'admin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_data`
--

CREATE TABLE `tbl_data` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_data`
--

INSERT INTO `tbl_data` (`id`, `name`, `description`) VALUES
(1, 'Game of thrones', '                                        \r\n         Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin''s series of fantasy novels, the first of which is A Game of Thrones.                       '),
(2, 'Flash', '                                        \r\n                  The Flash is an American television series developed by Greg Berlanti, Andrew Kreisberg and Geoff Johns, airing on The CW. It is based on the DC Comics character Barry Allen / Flash, a costumed superhero crime-fighter with the power to move at superhuman speeds              '),
(3, 'Prison Break', '                                        \r\n    Prison Break is an American television serial drama created by Paul Scheuring, that was broadcast on Fox for four seasons from 2005 to 2009, and a fifth season in 2017. The series revolves around two brothers, one of whom has been sentenced to death for a crime he did not commit, and the other who devises an elaborate plan to help his brother escape prison and clear his name                            '),
(4, '13 reasons why', '                                        \r\n                               13 Reasons Why (stylized onscreen as Th1rteen R3asons Why) is an American drama-mystery web television series based on the 2007 novel Thirteen Reasons Why by Jay Asher and adapted by Brian Yorkey for Netflix.[2] The series revolves around a high school student, Clay Jensen, and another student, Hannah Baker, who committed suicide after suffering a series of demoralizing circumstances, brought on by select individuals at her school. '),
(5, 'walking dead', '                The Walking Dead is an American horror drama television series developed by Frank Darabont, based on the comic book series of the same name by Robert Kirkman, Tony Moore, and Charlie Adlard. Andrew Lincoln plays the show''s lead character, sheriff''s deputy Rick Grimes,[3] who awakens from a coma discovering a world overrun by zombies, commonly referred to as "walkers".[4] Grimes reunites with his family and becomes the leader of a group he forms with other survivors.                        \r\n                                '),
(6, 'Person of Interest', '                                        \r\n                         Person of Interest is an American science fiction crime drama[1] television series that aired on CBS from September 22, 2011,[2] to June 21, 2016,[3] over five seasons, comprising a total of 103 episodes. The series was created by Jonathan Nolan, and executive produced by Nolan, J. J. Abrams, Bryan Burk, Greg Plageman, Denise Thé, and Chris Fisher.       ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_data`
--
ALTER TABLE `tbl_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_data`
--
ALTER TABLE `tbl_data`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
