-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2017 at 02:34 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_practise`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_full_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(50) NOT NULL,
  `admin_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_full_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'Disha', 'admin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_data`
--

CREATE TABLE `tbl_data` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_data`
--

INSERT INTO `tbl_data` (`id`, `name`, `description`) VALUES
(1, 'Game of thrones', '                                        \r\n         Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin''s series of fantasy novels, the first of which is A Game of Thrones.                       '),
(2, 'Flash', '                                        \r\n                  The Flash is an American television series developed by Greg Berlanti, Andrew Kreisberg and Geoff Johns, airing on The CW. It is based on the DC Comics character Barry Allen / Flash, a costumed superhero crime-fighter with the power to move at superhuman speeds              '),
(3, 'Prison Break 2', '                                                                                                                \r\n    Prison Break is an American television serial drama created by Paul Scheuring, that was broadcast on Fox for four seasons from 2005 to 2009, and a fifth season in 2017. The series revolves around two brothers, one of whom has been sentenced to death for a crime he did not commit, and the other who devises an elaborate plan to help his brother escape prison and clear his name                              \r\n                                  \r\n                                '),
(4, '13 reasons why', '                                        \r\n                               13 Reasons Why (stylized onscreen as Th1rteen R3asons Why) is an American drama-mystery web television series based on the 2007 novel Thirteen Reasons Why by Jay Asher and adapted by Brian Yorkey for Netflix.[2] The series revolves around a high school student, Clay Jensen, and another student, Hannah Baker, who committed suicide after suffering a series of demoralizing circumstances, brought on by select individuals at her school. '),
(5, 'walking dead', '                The Walking Dead is an American horror drama television series developed by Frank Darabont, based on the comic book series of the same name by Robert Kirkman, Tony Moore, and Charlie Adlard. Andrew Lincoln plays the show''s lead character, sheriff''s deputy Rick Grimes,[3] who awakens from a coma discovering a world overrun by zombies, commonly referred to as "walkers".[4] Grimes reunites with his family and becomes the leader of a group he forms with other survivors.                        \r\n                                '),
(6, 'Person of Interest', '                                        \r\n                         Person of Interest is an American science fiction crime drama[1] television series that aired on CBS from September 22, 2011,[2] to June 21, 2016,[3] over five seasons, comprising a total of 103 episodes. The series was created by Jonathan Nolan, and executive produced by Nolan, J. J. Abrams, Bryan Burk, Greg Plageman, Denise Thé, and Chris Fisher.       '),
(7, 'Person of Interest tv series', '                                                                                                                                                                                        \r\n是美國科幻小說，21，2016，[3]五個季節，共有103集。 系列是由喬納森·諾蘭創作，由諾蘭，J. 布萊恩·布克，格雷格Plageman，DeniseThé和Chris Fisher等人製作。  \r\n                                  \r\n                                '),
(8, 'Hello everyone', '                                        \r\n           sss                     '),
(11, 'Hello everyone', 'it is a great fun.                                                                                                                                                    \r\n               i like it     so much      sss. Its me        \r\n                                  \r\n                                  \r\n                                  \r\n                                '),
(15, 'hello world', '                                                                            \r\n            Codeigniter practise    is a great fun. you shoud try. you will definitely get fun. it is really enjoyable.Thank you.                  \r\n                                '),
(16, 'Supernatural (U.S. TV series)', 'Supernatural is an American fantasy horror television series created by Eric Kripke. It was first broadcast on September 13, 2005, on The WB, and subsequently became part of successor The CW''s lineup. Starring Jared Padalecki as Sam Winchester and Jensen Ackles as Dean Winchester, the series follows the two brothers as they hunt demons, ghosts, monsters, and other supernatural beings. The series is produced by Warner Bros. Television, in association with Wonderland Sound and Vision. Along with Kripke, executive producers have been McG, Robert Singer, Phil Sgriccia, Sera Gamble, Jeremy Carver, John Shiban, Ben Edlund and Adam Glass. Former executive producer and director Kim Manners died of lung cancer during production of the fourth season.[2]\r\n\r\nThe series is filmed in Vancouver, British Columbia and surrounding areas and was in development for nearly ten years, as creator Kripke spent several years unsuccessfully pitching it. The pilot was viewed by an estimated 5.69 million viewers,[3] and the ratings of the first four episodes prompted The WB to pick up the series for a full season. Originally, Kripke planned the series for three seasons but later expanded it to five. The fifth season concluded the series'' main storyline,[4] and Kripke departed the series as showrunner.[5] The series has continued on for several more seasons with new showrunners, including Sera Gamble, Jeremy Carver, Robert Singer and Andrew Dabb.[6][7][8] The series was renewed for an eleventh season,[9] which premiered on October 7, 2015.[10] With its eleventh season, Supernatural became the longest-running American fantasy series.[11] The series was renewed for a twelfth season,[12] which premiered on October 13, 2016.[13] On January 8, 2017, The CW renewed the show for a thirteenth season.[14]                                        \r\n                                '),
(17, 'Hello everyone', 'qqqqq                                     world wide fun.                                    www                                       \r\n                                  \r\n                                  \r\n                                ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(5) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_registration`
--

CREATE TABLE `tbl_user_registration` (
  `id` int(5) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `birthday` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_registration`
--

INSERT INTO `tbl_user_registration` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `profession`, `birthday`, `gender`) VALUES
(1, 'Disha', 'Sharmin', 'Shobnom', 'disha@gmail.com', '25d55ad283aa400af464c76d713c07ad', '112233', 'doctor', '05/02/2017', 'female'),
(2, 'jabbar', 'ahmed', 'khan', 'ahmed@gmail.com', '1122334455', '1111111111111', 'teacher', '06/23/2017', 'male'),
(4, 'jobbar', 'jobbar', 'khan', 'jobbar@gmail.com', '12345678', '01922', 'killer', '05/29/2017', 'other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_data`
--
ALTER TABLE `tbl_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_data`
--
ALTER TABLE `tbl_data`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
